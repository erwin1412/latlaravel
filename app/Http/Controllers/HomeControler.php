<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeControler extends Controller
{
    public function index3(){
        
        return view('data.index');
    }
    public function index(){
        
        return view('data.form');
    }
    public function index1(Request $REQUEST){
        $fn= $REQUEST['fn'];
        $ln= $REQUEST['ln'];
        return view('data.welcome' , ['fn' => $fn , 'ln' => $ln]);
    }
}
